import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Observable} from 'rxjs';
import {PokemonService} from '../../services/pokemon.service';

@Component({
  selector: 'pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent implements OnInit, OnChanges {
  @Input('pokemon') pokemon: string;
  name: string;
  detail$: Observable<any>;

  constructor(private pokemonService: PokemonService) { }

  ngOnInit(): void {
  }

  ngOnChanges({pokemon}: SimpleChanges): void {
    if (pokemon.currentValue !== '') {
        this.pokemonService.getPokemon(pokemon.currentValue).subscribe((data) => {
          this.name = data.name;
          this.detail$ = this.pokemonService.getPokemonSpecification(data.varieties.url);
        });
    }
  }

}

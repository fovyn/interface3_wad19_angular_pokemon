import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonRegionComponent } from './pokemon-region.component';

describe('PokemonRegionComponent', () => {
  let component: PokemonRegionComponent;
  let fixture: ComponentFixture<PokemonRegionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokemonRegionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

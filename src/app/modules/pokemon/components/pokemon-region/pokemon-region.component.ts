import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {RegionService} from '../../services/region.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'pokemon-region',
  templateUrl: './pokemon-region.component.html',
  styleUrls: ['./pokemon-region.component.scss']
})
export class PokemonRegionComponent implements OnInit {
  @Output('pokedex') selectedPokedexEvent = new EventEmitter<string>();
  regions$: Observable<Array<any>>;
  pokedexes$: Observable<Array<any>>;

  constructor(private regionService: RegionService) { }

  ngOnInit(): void {
    this.regions$ = this.regionService.Regions$;
  }

  selectRegion(regionUrl: string) {
    this.pokedexes$ = this.regionService.getRegionPokedex(regionUrl);
  }

  selectPokedex(pokedexUrl: string) {
    this.selectedPokedexEvent.emit(pokedexUrl);
  }
}

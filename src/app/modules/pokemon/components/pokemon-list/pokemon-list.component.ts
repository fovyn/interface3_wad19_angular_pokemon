import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {RegionService} from '../../services/region.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit, OnChanges {
  @Input('pokedex') pokedex: string;
  @Output('pokemon') selectPokemonEvent = new EventEmitter<string>();

  pokedexes$: Observable<any>;

  constructor(private regionService: RegionService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.pokedex.currentValue !== '') {
      this.pokedexes$ = this.regionService.getPokedex(changes.pokedex.currentValue);
    }
  }

  selectPokemon(url: string) {
    this.selectPokemonEvent.emit(url);
  }
}

import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss']
})
export class PokemonComponent implements OnInit {
  selectPokedex: string = '';
  selectPokemon: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  selectPokedexAction(pokedexUrl: string) {
    this.selectPokedex = pokedexUrl;
  }

  selectPokemonAction(pokemonUrl: string) {
    this.selectPokemon = pokemonUrl;
  }
}

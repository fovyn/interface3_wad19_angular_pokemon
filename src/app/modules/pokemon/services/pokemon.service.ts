import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private httpClient: HttpClient) { }

  getPokemon(pokemonUrl: string): Observable<any> {
    return this.httpClient.get<any>(pokemonUrl).pipe(map(data => ({name: data.name, varieties: data.varieties[0].pokemon})));
  }

  getPokemonSpecification(specificationUrl: string): Observable<any> {
    return this.httpClient.get<any>(specificationUrl).pipe(map(data => data.sprites));
  }
}

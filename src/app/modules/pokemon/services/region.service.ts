import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {flatMap, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegionService {

  constructor(private httpClient: HttpClient) { }

  get Regions$(): Observable<Array<string>> {
    return this.httpClient.get<any>('https://pokeapi.co/api/v2/region/').pipe(map(data => data.results));
  }

  getRegionPokedex(regionUrl: string): Observable<any> {
    return this.httpClient.get<any>(regionUrl).pipe(map(data => data.pokedexes));
  }

  getPokedex(pokedexUrl: string): Observable<any> {
    return this.httpClient.get<any>(pokedexUrl).pipe(map(data => data.pokemon_entries));
  }
}

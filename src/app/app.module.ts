import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import {PokemonModule} from './modules/pokemon/pokemon.module';
import { TabsButtonComponent } from './components/tabs-button/tabs-button.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { Tab1ComponentComponent } from './components/tab1-component/tab1-component.component';
import { Tab2ComponentComponent } from './components/tab2-component/tab2-component.component';

@NgModule({
  declarations: [
    AppComponent,
    TabsButtonComponent,
    TabsComponent,
    Tab1ComponentComponent,
    Tab2ComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PokemonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

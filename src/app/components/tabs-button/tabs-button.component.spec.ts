import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsButtonComponent } from './tabs-button.component';

describe('TabsButtonComponent', () => {
  let component: TabsButtonComponent;
  let fixture: ComponentFixture<TabsButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

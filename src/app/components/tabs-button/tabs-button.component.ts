import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'tabs-button',
  templateUrl: './tabs-button.component.html',
  styleUrls: ['./tabs-button.component.scss']
})
export class TabsButtonComponent implements OnInit {
  @Input() tab: string;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goTo() {
    console.log(this.tab, "Hello");
    this.router.navigate(['/tabs', this.tab]);
  }
}

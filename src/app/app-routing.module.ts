import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TabsComponent} from './components/tabs/tabs.component';
import {Tab1ComponentComponent} from './components/tab1-component/tab1-component.component';
import {Tab2ComponentComponent} from './components/tab2-component/tab2-component.component';


const routes: Routes = [
  {path: 'tabs', component: TabsComponent, children: [
      {path: 'tab1', component: Tab1ComponentComponent},
      {path: 'tab2', component: Tab2ComponentComponent}
    ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
